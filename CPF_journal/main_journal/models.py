from django.db import models


# Create your models here.


class Bb(models.Model):
    title = models.CharField(max_length=50, verbose_name='Название')
    content = models.TextField(null=True, blank=True,
                               verbose_name='Описание')
    published = models.DateTimeField(auto_now_add=True, db_index=True,
                                     verbose_name='Опубликовано')
    rubric = models.ForeignKey('Rubric', null=True, on_delete=models.PROTECT,
                               verbose_name='Комплектующие')

    class Meta:
        verbose_name_plural = 'Задания'
        verbose_name = 'Задание'
        ordering = ['-published']


class Rubric(models.Model):
    name = models.CharField(max_length=20, db_index=True,
                            verbose_name="Комплектующие")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Разделы'
        verbose_name = 'Раздел'
        ordering = ['name']
